imgName=$1
# lxc list returns string with headers if image is found.
# first grep returns just the line with the IP address out
# The first cut splits on | and returns the piece containing the IP
# awk trims leading whitespace
# last cut splits the IP from eth0 device string
(lxc list $imgName  | grep "$imgName" | cut -d "|" -f 4 | awk '{$1=$1};1' | cut -d " " -f 1)
