#!/bin/bash
# scripts/container/forward_port_host_to_container.sh
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/.."
exampleBase="$scriptBase/../example"
source $scriptBase/utility_functions.sh


contName=$1
hostPort=$2
contPort=$3
delim="_"
proxyName="$contName$delim$hostPort$delim$contPort"

# GET Container IP Address
hostIP=$(bash $scriptBase/get_host_ip.sh)
#TODO: ADD Check to aport if no IP Address


# GET Server / host IP Address
contIP=$(bash $scriptBase/container/get_container_ipv4.sh $contName)
echo "forward port $hostPort on $hostIP to port $contPort on $contIP for $contName proxyName=$proxyName"

#TODO: Check to make sure we got a contIP and if not then 
# wait for IP.

#echo "lxc config device add $contName $proxyName proxy listen=tcp:$hostIP:$hostPort connect=tcp:$contIP:$contPort bind=host"

 trun=$(lxc config device list $contName)
 echo "trun=$trun"
 if grep $proxyName  <<< $trun
 then 
    echo "proxyName $proxyName already exists so removing"
    lxc config device remove $contName $proxyName
    tres=$?
    # TODO: Check for bad remove and aport
 fi
    
#TOOD: We should try to find out if the port is available 
# before we start forwarding it.
# Searched could not find out how to do this so need to 
# record those we have forwarded so we can reference it
# in a file. 

tout=$(lxc config device add $contName $proxyName proxy listen=tcp:$hostIP:$hostPort connect=tcp:$contIP:$contPort bind=host)
tres=$?
# TODO: Check for bad add and abort
echo "lxc res Code: $tres  result: $tout"

exit 0
