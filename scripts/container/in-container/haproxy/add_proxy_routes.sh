#!/bin/bash
# scripts/container/in-container/add_proxy_routes.sh"
# Add the proxy config in the specified input file to the
# end of the haproxy configuration file.
#
# See Also: https://blog.codeship.com/performance-tuning-haproxy/
#
srcFi=$1
uriTestPath=$2
uriTestString=$3

echo "$0  srcFi: $srcFi"
cwd=$(pwd)
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
configFi="/etc/haproxy/haproxy.cfg"
configFiTmp="$configFi.tmp"
listenPort=$(cat $srcFi | grep bind | cut -d":" -f2)

echo "$0  srcFi: $srcFi  configFi: $configFi  configFiTmp: $configFiTmp listenPort: $listenPort"
if [ -f $srcFi ]; then
   echo "$0 srcFi $srcFi  exists "
else
   echo "ABORT: $0 srcFi: $srcFi does not exist"
   exit 1
fi

if [ -f $configFi ]; then
   echo "$0 configFi: $configFi  exists "
else
   echo "ABORT: $0 configFi: $configFi does not exist"
   exit 1
fi


# Concatate the two files together producing a 3rd file
# then replace the original file. 
cp $configFi "$configFi.bak"
cat $configFi $srcFi > $configFiTmp
cp $configFiTmp $configFi


# Check config to see if it passes syntax check by haproxy
sudo haproxy -c -f $configFi
if [ "$?" = "1" ]; then
  echo "ABORT: $0 syntax check on $configFi failed"
  exit 1
fi

# Restart the service with the new loadbalancer config
sudo systemctl restart haproxy
if [ "$?" = "1" ]; then
  echo "ABORT: $imgName failed restart of haproxy"
  exit 1
else
  echo "$0 sucessful restart of  haproxy"
fi
sleep 2

curlRes=$(curl -s "http://127.0.0.1:$listenPort$uriTestPath")
foundStr=$(echo "$curlRes" | grep "$uriTestString")
if [ "$?" = "1" ]; then 
  echo "ABORT: $0 curlResult from $uriTestPath did not contain $uriTestString got $curlRes"
  echo "Display resulting config"
  tail -n100 $configFi
  exit 1
fi
