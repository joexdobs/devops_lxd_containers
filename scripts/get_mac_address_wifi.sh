#!/bin/bash
# Returns local MAC address for default Ethernet
# Requires sudo apt  install network-manager 
# WARNING: Ethernet device naming is complex and has considerable variability
#  This script may need to be adjusted to reflect local server device names.
echo $(nmcli | grep wifi)