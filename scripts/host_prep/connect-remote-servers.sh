#!/bin/bash
# connect-remote-servers.sh
#  Demonstration of connecting remote servers
#  without creating a cluster 

if [ "$#" -lt "1" ]; then
  msg="ABORT: $1  invalid number of arguments
  
  [1] - password used to connect to remote servers 

  returns 0 response code if success otherwise 1
       
  Example: 
    bash $1  IAmAPassword
  "
  # Send msg to stderr
  1>&2 echo "$msg"
  exit 1
fi

# http://manpages.ubuntu.com/manpages/bionic/man1/lxc.remote.add.1.html
# http://manpages.ubuntu.com/manpages/cosmic/man1/lxc.remote.add.1.html
# https://wiki.gentoo.org/wiki/LXD

##################
## WARNING THESE ARE MY HOSTS ON A PRIVATE NETWORK
## YOU WILL NEED TO SUPPLY YOUR OWN NAMES AND IP ADDRESSES
##################
pass=$1
lxc remote add brix1 192.168.1.102 --password $pass
lxc remote add brix2 192.168.1.103 --password $pass


# When the lcx remote add asks if it can accept the cert from the
# remote server then answer yes.  I have not found a way to answer yes
# automatically. 
