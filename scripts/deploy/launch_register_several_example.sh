#!/bin/bash
# scripts/deploy/launch_and_register_test.sh
# Shows how to operate the launch_and_register.sh script
# to create a VM from a named image map it's ports so they
# can be accessible outside the host. 
#
# This is the brute force approach.  We have other deployment
# Scripts that will control the port mapping to avoid
# conficts.  This is also doing all the work on one host.
# the more complex version would use remot servers to host
# the different instances.
#
#
# Directory where this script exists
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/.."
exampleBase="$scriptBase/../example"

# For each server we grab ports sequentially out of the 
# range of 8100 and map them to port 22 to allow SSH access
# For each server we allocate ports sequentially out of 
# range of 8200 and map them to port 80
# Nothing special in this mapping scheme but it is 
# similar to how our automated manager will work.


# Launch my Apache Fleet
bash $scdir/launch_and_register.sh  apacheKVkvsample   apacheKVConfig01  "NONE:8200=22,/kv:8100=80" mapped.ip.txt noscript
bash $scdir/launch_and_register.sh  apacheKVkvsample   apacheKVConfig02  "NONE:8201=22,/kv:8101=80" mapped.ip.txt noscript

# Launch my Apache Generic Fleet
bash $scdir/launch_and_register.sh  basicApache   basicApache01  "NONE:8202=22,/web:8102=80" mapped.ip.txt noscript
bash $scdir/launch_and_register.sh  basicApache   basicApache02  "NONE:8203=22,/web:8103=80" mapped.ip.txt noscript


# Launch My HA Proxy Fleed
bash $scdir/launch_and_register.sh  basicHAProxy   basicHAProxy01  "NONE:8204=22,/api:8104=80" mapped.ip.txt noscript
bash $scdir/launch_and_register.sh  basicHAProxy   basicHAProxy02  "NONE:8205=22,/api:8105=80" mapped.ip.txt noscript

echo "Contents of mapped.ip.txt"

echo "###############"
echo "Here are the last 100 lines of mapped.id.txt generated"
echo "###############"
tail -n 100 mapped.ip.txt
