#!/bin/bash
# spinup_kafka_server.sh

#WIP - UNDER CONSTRUCTION

Cmt="
  Spinup a number of Kafka Server Containers, Pass them sufficient information 
  to establish the enviornment they are running in.  Spread  the containers 
  across the available hosts.  Forward the logging to a specific remote syslog
  host. 
  
  [1] - Template to Launch 
  
  [2] - ENV - Enviornment name to pass to instances.  This modifies where they
      will look in the KV config store.  It is also included in most log messages.
  
  [3] - Host List - A comma delimited file containing the list of hosts where
     you want to deploy the Kafka container.  LXDName=HostIP The left hand side
     is the LXD alias assigned during the attach for that host.  The right hand
     side is the IP address the ports will be exposed on for the container
     
  [4] KV config repository sources
  
  [5] sysLog Destination
  
  Assumptions:
    One Kafka server will run on a single host. This is intended to
    avoid Conflicts for ports and data locations.
    
    We will start one Kafka server and one zoo keeper server for each 
    server in host list.
    
    We will use the same data directory on each host as the mapped 
    drive.  See kafka-host-prep.sh
    
    Number of Containers will equal number of hosts you deployed Kafka
    containers on. 
    
    Prior to running this script you have used the lxc attach command 
    add added each host with an alias that matches what is specified
    in the command. 
    
    Assumes our placement utility has decided which hosts on prior to 
    running here.  This placement is what determined the list of 
    hosts passed in. 
    
    ssh certs have been installed that allow ssh access without login to each 
    host to setup log and data directories.
    
EXAMPLE:
  bash $0 basicKafka TEST \"brix1=192.168.1.102,brix2=192.168.1.100,brix3=192.168.1.106\" http://kv.abc.com logs.apc.com
  
"
# https://gist.github.com/mkanchwala/fbfdd5ef866a58a77f6e 

template=$1
ENV=$2
hostList=$3
kvSrc=$4
logDest=$5

hostId=0
numHost=0
portMap="NONE:2181=2181,NONE:2888=2888,NONE:3888=3888"


for EACH in `echo "$hostList" | grep -o -e "[^,]*"`; do
    let numHost=numHost+1        
done
echo "found $numHosts hosts"


for EACH in `echo "$hostList" | grep -o -e "[^,]*"`; do
    echo "Found: \"$EACH\""
    let hostId=hostId+1    
    lxdHost=$(echo $EACH | cut -d '=' -f 1)
    hostIP=$(echo $EACH | cut -d '=' -f 2)    
    kafkaContName="$lxdHostKafka$hostId"
    echo "L82: hostId=$hostId lxdHost=$lxdHost hostIP=$hostIP kafkaContName=$kafkaContName"    
    bash -x spinup_kafka_server.sh basicKafka $kafkaContName  $lxdHost  $ENV $hostId  $kafkaContName $hostList  $kvSrc  $logDest 
    
done


