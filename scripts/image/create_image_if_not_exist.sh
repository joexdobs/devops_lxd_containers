#!/bin/bash
# Check to see if the named image exists and if not then run the 
# script that creates it. If createScript (parm 2) is not specified
# then skips.  Returns error code 0 if sucessful.  Returns 1
# if create script could not be found.

checkImg=$1
createScript=$2

if (lxc image list $checkImg | grep " $checkImg .*|"); then
  echo "image $chekImg exists"
else
  if [ -n "$createScript" ]; then
    if [ -f $createScript ]; then
     echo "image $chekImg does not exist, Run create script $createScript"
     bash $createScript
     # TODO: Check Result of produce parent script so we can pass
     # to our caller.      
     exit 0
    else
       echo "ABORT: Template $checkImg does not exists and create script $createScript not found."
       exit 1
    fi
  else
    echo "image $chekImg does not exist but no create script specified"
    exit 0
  fi
fi
