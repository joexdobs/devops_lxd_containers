#!/bin/bash
# Produce a fully updated Ubuntu Image publish it as updatedUbuntu
# This script needs to run as sudo because the launch requires sudo.

# Save script Dir so we can make sub script calls relative
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

imgName=basicUbuntu
publishImg=updatedUbuntu
# Full server Image
template=ubuntu:18.04

#####
## Enable these to use the Minimal Ubuntu Images
## Instead of the default server Images
#####
# Allow us to find the minimal images
lxc remote add --protocol simplestreams ubuntu-minimal https://cloud-images.ubuntu.com/minimal/releases/
# Minimal server image boots in about 1/2 the RAM
#template=ubuntu-minimal:18.10
template=ubuntu-minimal:18.04

source $scdir/produce_image_setup.sh

bash $scdir/../../container/update_existing_packages.sh $imgName

# Add zip so we can unzip our file bundles
lxc exec $imgName -- apt install unzip

# Install gnu awk because it allows in-place editing.  Ensure we have the basic packages we need even when 
# using minimal images. 
lxc exec $imgName -- apt-get install gawk curl wget python unzip zip openssh-server openssh-client gzip tar sed grep -y

# Set our default ENV variable so we know this is a specific image.
# To do this we use some helper scripts we will download from this repository.

# NOTE: Downloading with curl is not really recommended
#   Only showing here to illustrate the concept.   If the file
#   is already included in the repository or available on the host
#   we can move it using the LXC push command.

echo "download add_if_missing.sh"
lxc exec $imgName -- curl https://bitbucket.org/joexdobs/devops_lxd_containers/raw/master/scripts/container/in-container/add_if_missing.sh -o "add_if_missing.sh"


echo "Download  add_default_env.sh"
lxc exec $imgName -- curl https://bitbucket.org/joexdobs/devops_lxd_containers/raw/master/scripts/container/in-container/add_default_env.sh -o "add_default_env.sh"                          
                          

#  run the script to set the default ENV variable
# for root
#echo "Add default ENV to $imgName"
lxc exec $imgName -- bash add_default_env.sh 

# Run script to add ENV variable to all environment
# when they start
lxc config set $imgName environment.ENV BUILDIMG
lxc config set $imgName environment.KVCONFIG "http://kvconfigstore./--ENV--.--DOMAIN--"
echo "Add ENV=BUILDIMG for all users"
lxc exec $imgName -- bash add_if_missing.sh ENV ENV=\"BUILDIMG\" /etc/environment

# Add the URI where we can expect to find our config store.
# So all shells should inherit it.  Notice the {ENV} which
# intended to be expanded at runtime with the value in the
# ENV environment variable.
echo "Add KVConfig environment for all users"
lxc exec $imgName -- bash add_if_missing.sh KVCONFIG KVCONFIG=\"http://kvconfigstore.\{ENV\}.abc.com\" /etc/environment



#TODO: NEED TO SEE IF THE .bash value saved in 
#  the environment variable worked and is set for the 
#  environment the service is reading. 
bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg


