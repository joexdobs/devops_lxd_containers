# temp.py

def readServerList(fiName):
  tout={}
  with open(fiName) as fp:  
    while True:
       line = fp.readline()
       if not line:
         break
       line = line.strip()
       if len(line) < 1:
         continue
       if line.startswith("#"):
         continue
       tarr = line.split(" ",1)
       if len(tarr) == 2:
         tout[tarr[1].strip()] = tarr[0].strip()     
  return tout


def readProperties(fiName):
  tout={}
  with open(fiName) as fp:      
    while True:
       line = fp.readline() 
       if not line:
          break
       line = line.strip()
       if len(line) < 1:
         continue
       if line.startswith("#"):
         continue       
       tarr = line.split("=",1)
       akey = tarr[0].strip()
       aval = None
       if len(akey) < 1:
         continue
       if len(tarr) == 2:
         aval = tarr[1].strip()         
         try:
           nval = int(aval)
           aval = nval
         except ValueError as verr:
            pass # Not a int
         except Exception as ex:
            pass # Not a int          
       tout[akey] = aval
  return tout


def readTemplatePortMap(fiName):
  tout={}
  with open(fiName) as fp:      
    while True:
       tobj = {}
       line = fp.readline()     
       if not line:
           break
       line = line.strip()
       print "line=", line
       if len(line) < 1:
         continue
       if line.startswith("#"):
         continue       
       fldarr = line.split(",") 
       print "fldarr=", fldarr
       for fldstr in fldarr:
         print "  fldstr=", fldstr
         tarr = fldstr.split("=",1)
         akey = tarr[0].strip()
         print "   akey=", akey
         aval = None
         if len(akey) < 1:
            continue
         if len(tarr) == 2:
            aval = tarr[1].strip()         
            try:
               nval = int(aval)
               aval = nval
            except ValueError as verr:
               pass # Not a int
            except Exception as ex:
               pass # Not a int          
         tobj[akey] = aval
       # Take obj key from name template 
       # and convert array of sub objects
       print "tobj=", tobj
       templName = tobj["template"]  
       print "templName=", templName
       if templName in tout:
         tout[templName].append(tobj)
       else:
         tout[templName] = [tobj]
  return tout
