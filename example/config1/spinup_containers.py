# spinup_hosts.py
#
# Python code to sin up hosts 
# 
# Generates a shell script launch a bunch of Hosts
# on different machines using he specified container names
# List of requesed mappings is supplied in node_map.json
# container port mapping is supplied by template_port_map.json
# The mapping of machine names to IP addresses is supplied by
# Host_list.json
#
# TODO: Demo this with a enviornment where requested # of hosts
#  change based on an enviornment variable. Need script that reads
#  file and sets local enviornment variables following file2console
#  pattern.
#

import sys
import os
import json
import os


def fixFiName(astr):
  return astr.replace("\\", "/").replace("//", "/")

def join(seg1, seg2):
  return os.path.normpath(fixFiName(os.path.join(seg1.strip(), seg2.strip()))).replace("\\", "/")

def readJSONFile(fiName):
    print "readJSONFile", fiName
    with open(fiName) as fp:   
        txt = fp.read()
        return json.loads(txt)
      
      
# Interpolate variables from enviornment into current file before it 
# is parsed by JSON.  Once done then parse it into a JSON string.  
# Allows passing in multiple dictionaries as a list so we can lookup
# interpolation in both the enviornment from the OS and dictionaries locally
# defined. 
def interpolateThenReadJSONFile(fiName, envDicts):
  pass


def emptyHostUseRec():
    return { "ramGUsed" : 0, "diskGUsed" : 0,
             "cpuCntUsed" : 0, "iopsUsed" : 0,
             "contCnt" : 0, "portsUsed" : {}}
    
class Hosts:
  def __init__(self, planner):
    self.planner = planner
    self.srcDir = planner.srcDir
    self.workDir = planner.workDir
    self.argv = planner.argv
    srcDir=self.srcDir
    workDir=self.workDir
    self.startPort = planner.commonHostProp["listenPortRange"]["start"]
    self.endPort = planner.commonHostProp["listenPortRange"]["end"]
    
    # List of available Hosts indexed by
    # name with their IP address 
    self.hostListFi = join(srcDir, "host_list.json")
    #print "hostListFi=***", self.hostListFi, "***"
    self.hostUseFi = join(workDir, "host_usage.json")    
    self.hostProp    = readJSONFile(self.hostListFi)
    self.hostsByName = self.hostProp["hosts"]
    self.hostUse = {}
    self.hostsBySecZone = {}
    self.templatesBySecZone = {}
    self.hostsByAvailZone = {}
    self.secZones={}
    self.availZones={}
    self.hostBusyNdx={}
    self.portsUsed={}
    # A simple counter of host business by
    # number of containers running on the host
    # This is designed to allow new algorithms
    # that as long as they can produce a single number
    # to use for sorting can be swapped for this list
    # this can allow us to build a new index based on
    # host business.  If any host exceeds 80% of available
    # memory or # of cores or 70% of avaialble disk then
    # they will be marked as very busy and will receive
    # no additional work until all servers are equally as
    # busy. 
    initHostUseFlg = True
    if os.path.isfile(self.hostUseFi):
      self.hostUse = readJSONFile(self.hostUseFi)
      initHostUseFlg = False

    #print "self.hostsByName=", self.hostsByName
    #print "self.hostsByName.keys()=",self.hostsByName.keys()
    # Build our fast access indexes
    for servName in self.hostsByName:
      servRec = self.hostsByName[servName]
      servRec["name"] = servName
      # Update Host index by Avail Zone
      availZone = servRec["availZone"]
      if not availZone in self.hostsByAvailZone:
          self.hostsByAvailZone[availZone] = {}
          self.availZones[availZone] = 0
      self.availZones[availZone] += 1
      self.hostsByAvailZone[availZone][servName] = servRec
      self.hostBusyNdx[servName]=0
      
      # Update index by Availability Zone
      for secZone in servRec["secZones"]:
          if not secZone in self.hostsBySecZone:
             self.hostsBySecZone[secZone] = {}
             self.secZones[secZone]=0
          self.hostsBySecZone[secZone][servName] = servRec
          self.secZones[secZone] += 1
          
      # Build a default index for Host Usage
      if not servName in self.hostUse:
        self.hostUse[servName] = emptyHostUseRec()
      self.makeIndexOfAllSecZoneReferencedByTemplates()
      self.updateSecZonesForHostsWithAll()

  def makeIndexOfAllSecZoneReferencedByTemplates(self):
    """ Return an index of templates by security zone
     we need this because in the updateSecZonesForAllHostsWithAll
     we need to know the list of all secZones needed by
     any tmeplate. """
    tsz = self.templatesBySecZone
    templates = self.planner.templateInfo    
    for templateName in templates:
      templateInf = templates[templateName]
      secZone = templateInf["secZone"]
      if not secZone in tsz:
        tsz[secZone] = {}            
      if not templateName in tsz[secZone]:
        tsz[secZone][templateName] = templateInf
    #print "tsz=", tsz

    
  def updateSecZonesForHostsWithAll(self):
    """ Update each secZone to in "all" zone our servers
    add that server to the list for servers
    approved for every zone mentioned in any
    template. """
    tsz = self.templatesBySecZone
    hostsBySecZone = self.hostsBySecZone
    #print "L142:hostBySecZone=", hostsBySecZone 
    secZoneAllServ = hostsBySecZone["all"]
    secZones = self.secZones                       
    for hostName in secZoneAllServ:
      hostRec = secZoneAllServ[hostName]
      for secZone in tsz:
        if not secZone in hostsBySecZone:
          hostsBySecZone[secZone] = {}
          secZones[secZone] = 0
        if not hostName in hostsBySecZone[secZone]:
          hostsBySecZone[secZone][hostName] = hostRec
          secZones[secZone] += 1
    #print "L153:hostBySecZone=", hostsBySecZone 
            
  def getUsage(self,hostName):
      return self.hostUse[hostName]

  def getHostProp(self,hostName):
      return self.hostProp[hostName]

  def getServInSecZone(self, secZone):
    pass

  def getServInAvailZone(self, availZone):
    pass

  def getLeastUsedHost(self, availZone, secZone):
    pass

  def addHostUsage(self, hostName, templateInfo):
    # TODO: Harvest things like RAM, CPU, Storage from
    # template Info so we can use a better notion of busy
    # than a simple host count.
    if hostName in self.hostBusyNdx:
      self.hostBusyNdx[hostName] += 1
    else:
      self.hostBusyNdx[hostName] = 0
      

  def hostListByLoad(self, availHosts):
     tsorted = []
     #print "self.hostBusyNdx=", self.hostBusyNdx
     # Build a sort list  based on load factors
     for hostName in availHosts:
         print "hostName=", hostName
         sortRec = (self.hostBusyNdx[hostName], hostName)
         tsorted.append(sortRec)          
     # Sort the load list into lead load first.
     def getKey(item):
       return item[0]
     return sorted(tsorted, key=getKey)

  def getLeastLoadedHosts(self, secZone, numReq, minAvailZone, hostLoadNdx):
    """ Return the numReq hosts that match secZone and force them to
    be spread across at least minAvailZone.  To do this we rank order
    servers by amount of busy.  We then pull the least busy server
    in order working from lest busy towards most busy.
    We are always forcing the next server to be selected from
    a different availability zone until we run out of availability
    zones and all servers remaining are in the same availability
    zone when we will allow the next server to be chosen from a
    the same availability zone.   We continue this until we
    run out of servers then will allow the same template to be
    selected on the same host again to meet the numReq.    
    """
    tout = []
    availHosts = self.hostsBySecZone[secZone]
    #print "availHosts=", availHosts        
    busySort = self.hostListByLoad(availHosts)

    # Keep cycling through the list seeking the least used
    # server and adding to the list. But we can not choose
    # the server in the zone until we have cycled through
    # the number of items in the list which means we will
    # have to choose another in the same zone. 
    lastZone = -99
    cntSinceChoose = 0
    tryCnt=0
    while True:
      item = busySort.pop(0)
      hostName = item[1]
      hostRec=self.hostsByName[hostName]
      if hostRec['availZone'] != lastZone:
        tout.append(hostRec)
        # Remember this hosts availability zone so
        # we can avoid choosing a server until we find
        # one in a different zone. 
        lastZone = hostRec["availZone"]
        cntSinceChoose = 0
        restCnt = 0
        if len(tout) >= numReq:
          break
      else:
        # Could not add since this server is in the same
        # availability zone so we will add it back to the end
        # of the list to re-use if we run out. 
        busySort.append(1)
        cntSinceChoose += 1
        if cntSinceChoose >= (len(busySort) + 1):
          # If we have cycled through the entire
          # list since last choosing we reset our
          # last zone to allow rechoosing from same zone.
          print "resetting to allow next server random availability zone"
          lastZone = -99
          resetCnt += 1
          if resetCnt > 2:
            # We have been looping for a while so we
            # need to re-build our sort index to allow
            # us to choose the a server we already added
            # in a prior pass. 
            print "ran out of candiates so trying same servers again"
            busySort = makeLoadSorted()
    
    #print "L:247: HostsSelected=", tout
    return tout
  

  def saveAll(fiName):
    pass

  def saveAllHostUsage(fiName):
    pass

  def saveHostUsage(fiName):
    pass

  def pickUnusedPort(self, hostName):
    portsUsed = self.portsUsed
    if not hostName in portsUsed:
       aport = self.startPort
       portsUsed[hostName] = { aport : True}
       return aport
    else:
      # As we move loads around and deregister hosts we
      # will end up with holes in our port matrix so
      # we will need to simply scan to find a unused port
      # a better solution will be to index a list of unused
      # ports at startup and then we can remove and add items
      # as they become available. This only gives us a win if
      # we end up doing a lot of scans or live for a long process
      # life.
      hpu = portsUsed[hostName]
      for aport in range (self.startPort, self.endPort):
        if aport not in hpu:
          hpu[hostName] = True
          return aport
      return None




class WorkPlanner:
  
  def __init__(self, argv):
    self.argv=argv
    self.srcDir=argv[1]
    self.workDir=argv[2]
    self.baseDir=argv[3]
    
    # Used to interpolate files as they are read
    # If you want a custom dict to take precendent
    # then insert to front of dict. They are 
    # used in the order listed and processing stops
    # on first match. 
    self.interpolateDicts = [os.environ]
    dataDir = self.srcDir

    
    # Mapping of template to ports and layer 7 routing
    # prefix.  This is set at the template level since
    # we do not want to repeat it for every node_map
    self.templateInfoFi = join(dataDir, "template_info.json")

    #Common properties defined for all hosts
    self.commonHostPropFi = join(dataDir, 
"host_common_properties.json")

    # List of Nodes we wish to spin up containers 
    # on to represent one full enviornment.
    self.nodeMapFi=os.path.join(dataDir, "node_map.json")
    self.grpCfgFi = os.path.join(dataDir, "group_config.json")
    
    # Load the basic Data Structures
    #self.hostProp=readJSONFile(self.hostListFi)
    #self.hosts = self.hostProp["Hosts"]
    
    self.grpCfg = readJSONFile(self.grpCfgFi)
    self.commonHostProp = readJSONFile(self.commonHostPropFi)
    self.templateInfo = readJSONFile(self.templateInfoFi)
    self.nodeMap= readJSONFile(self.nodeMapFi)
    self.hosts = Hosts(self)
    
    print "Hosts=", self.hosts.hostProp
    print "commonHostProperties=", self.commonHostProp
    print "templateInfo=", self.templateInfo
    print "nodeMap=",self.nodeMap


  def interpolateStr(self, aStr):
    return interpolateStr(aStr, self.interpolateDicts)
  
  def addPortUsed(self, hostName, portsUsed, aport):
    sport = None
    if not hostName in portsUsed:
        sport = {}
        portsUsed[hostName] = sport
    else:
        sport = portsUsed[hostName]

    sport[aport] = True


 

  def generateNodeMap(self, grpCfg):
    """ Produce a Node Map processing the grp_cfg
    file.  The grp_cfg knows what templates we want
    to launch and it knows what ports need to be 
    mapped for each container. It also knows what
    ports are available for the host from ports_used
    If uses this data to allocate container to hosts
    and build the underlying port map. This file can 
    be used to produce the launch script latter. 
    """
    nodesOut = []
    tout = { "start" : nodesOut}
    hosts = self.hosts
    start = grpCfg["start"]
    for ndin in start:
      numReq = ndin["numReq"]
      template = ndin["template"]
      contName = ndin["contName"]
      minAvailZone = ndin["minAvailZone"]
      newNode = { "template" :  template,
               "contName" :  ndin["contName"],
               "postStartScript" : ndin["postStartScript"],
               "hosts" : [] }
      nodesOut.append(newNode)
      if not template in self.templateInfo:
        print "ERROR: Template: ", template, " not found in ", self.templateInfoFi
        continue
               
      nodeInfo = self.templateInfo[template]
      # Pick out the Hosts
      secZone = nodeInfo["secZone"]
               
      targetHosts = hosts.getLeastLoadedHosts(secZone, numReq, minAvailZone, hosts.hostBusyNdx)
      # Add the details for each of the ports the
      # host selector chose for us.
      for ahost in targetHosts:
        #print "ahost=", ahost
        hostName = ahost["name"]

        hosts.addHostUsage(hostName, nodeInfo) 
        newHostRec = {
          "name" : ahost["name"],
          "ip" : ahost["ip"],
          "ports" : []
          }
        newNode["hosts"].append(newHostRec)
        newPorts = newHostRec["ports"]
        # Add our the mapping for each of our ports
        # for this host.
        templatePorts = nodeInfo["ports"]
        for templatePort in templatePorts:
           hostPort = hosts.pickUnusedPort(hostName)
           # TODO: IF the hostPort is specified and available then
           # use it as a hint. But feel free to use a different
           # port if there is a conflict.
           newPortSpec = { "contPort" : templatePort["contPort"],
                           "hostPort" : hostPort }
           if "urlPrefix" in templatePort:
             newPortSpec["uriPrefix"] = templatePort["uriPrefix"]
           newPorts.append(newPortSpec)
           
        # Copy over the uriPrefix when approapriate
      
    return tout
  
  
  def genLaunchFiles(self, nodeMap, dstFiName):
    """  Once we know how we want to allocate work 
     to the various nodes we can then generate the 
     the shell script that can do the underlying work.
     This scripts accepts a computed nodeMap and 
     Generates that script. Saves the result into
     dstFiName
    """
    scriptOut = open(dstFiName, "a")      
    templateInfo = self.templateInfo
    hostsByName=self.hosts.hostsByName
    callName = join(self.baseDir, "scripts/deploy/launch_and_register.sh")
    portMapSaveFi = join(self.workDir, "mapped.ip.txt")
    for areq in self.nodeMap["start"]:
        #print "areq=", areq
        tempName = areq["template"]
        contName = areq["contName"]
        hostsReq  = areq["hosts"]
        if not tempName in templateInfo:
          print "Can not find ", tempName, " in ", self.templateInfoFi
          continue
        #print "hostsReq = ", hostsReq
        tempInf = templateInfo[tempName]
        #print "tempInf=", tempInf
        for reqHost in hostsReq:
            #print "reqHost=", reqHost
            reqHostName = reqHost["name"]
            #print "reqHostName=", reqHostName
            if not reqHostName in hostsByName:
              print "could not requested host ", reqHostName
            else:
              hostInf = hostsByName[reqHostName]
              #print "hostInf=", hostInf
              servIP = hostInf["ip"]
              ports = reqHost["ports"]
              #print "ports=", ports              
              portStrArr = []
              for portReq in ports:
                  #print "portReq=", portReq
                  uriPrefix = "NONE:"
                  if "uriPrefix" in portReq:
                    uriPrefix = portReq["uriPrefix"]                      
                  portStrArr.append( uriPrefix + ":" + str(portReq["hostPort"]) + "=" + str(portReq["contPort"]))
                  
              portReqStr = "\"" + ",".join(portStrArr) +  "\""
              #print "portReqStr=", portReqStr                                            
              tstr = ("bash " + callName + " " + tempName + " " +  contName
                      + " " + portReqStr + " " + portMapSaveFi                       
                      )
                
              #print "tstr=", tstr
              #servPort = getPortForHost(aHost)
              scriptOut.write(tstr)
              scriptOut.write("\n")
              
              if "L7" in tempInf and tempInf["L7"] == True:
                  print "No L7 for this Host for this template"
              else:
                  print "doing L7 routing"
                   
    scriptOut.close()              
                  
##################
## START MAIN ####
##################      

print(os.environ['HOME'])

wp = WorkPlanner(sys.argv)
wp.genLaunchFiles(wp.nodeMap, "temp.sh")


newNodeMap = wp.generateNodeMap(wp.grpCfg)
print "newNodeMap=", newNodeMap
wp.genLaunchFiles(newNodeMap, "temp.new.sh")
                           
# NOW: Lets write our new launch scripts based on what has changed.
                           
                           




    
    
    
