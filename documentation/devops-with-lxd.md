



# DevOps with LXD - Conceptual Guide

We have broken down  DevOps with lxd into two main sections because mixing the intro examples and the script usage was confusing for some readers.

- **This page** explores the major principals such as load balancers,  image creation, proxy setup, etc and shows in detail how each concept can be realized.   I provide bash style code snippets that can be cut and pasted one step at time and so they can be combined easily for full automation scripts.

- **[DevOps lxd Automation](devops-lxd-automation.md)** -  Explains how to use the DevOps automation code we have written to fully automate to deliver "infrastructure as code" deployments where new environments can be deployed with minimal effort. 

  See also [linuxcontainers.org](https://www.google.com/search?q=lxd+tomcat+script&rlz=1C1CHBF_enUS741US741&oq=lxd+tomcat+script&aqs=chrome..69i57.6071j0j7&sourceid=chrome&ie=UTF-8)

## Table of Contents

[TOC]

## Summary

This page looks at using LXD on Ubuntu servers as an alternative to save money.  It is built into Ubuntu which means Ubuntu Linux can be installed on bare metal then we synchronize deployment of workloads by deploying LXD containers.   The primary alternatives would be Docker or kubernetes with Rancher.    In this instance we will deploy the core functionality using a series of GO scripts.  

## Why LXD

[LXD Containers](https://linuxcontainers.org/lxd/introduction/) provide a unique container experience with some critical advantages the resonate with our [Devops guiding principals](devops-guiding-principals.md)   Some of these are:

* Low cost only the cost of the Ubuntu server
* Build from ground up for control by scripting
* Built in, easy to install,  Easy to get running on minimal hardware
* Low overhead - Minimal host OS resource wastage 
* Runs directly on a pre-hardened host OS making secondary hardening easier.
* Superior Isolation - Isolation on par with Docker or better in some instances.
* Widely available on many [linux distributions](https://us.images.linuxcontainers.org/)
* Easily saved images after customization for fast boot of additional images
* Easy to spin up 2 to 2000 containers from single host.
* REST API for distributed control of containers remotely.
* Advanced resource control [cpu, memory, netowrk I/O, disk usage, etc.]
* Storage management with multiple pools and storage volumes.

[LXD Container Commands Overview](lxd-commands-overview.md)

## Basic Approach

To Use LXD as the center piece of a Devops strategy The following need to be accomplished we will look how to accomplish this in the steps below.

* Configure Base OS servers -  We will assume that the core Ubuntu servers have been configured,   They have been added to the network and that can reach them via SSH.    A good choice for configuring many Linux bare metal servers can be [MAAS](https://maas.io/) but it is not difficult to control them  manually.      

* Provide each server with one or more IP addresses.

* Register servers with Devops Stack by Enviornment

* Demonstrate ability to create / provision servers using LXD/LXC scripts.

* Demonstrate ability to move workloads to best user servers.

* Demonstrate ability to honor network security segments when placing services.

* Demonstrate ability to support full life cycle rolling deploys and go-live after tests good.   Ability to defer adding to VIP when healthy.


In an OpenStack world we will divide the project into two pieces.  

- A)  Get OpenStack working,  
- B) Get to where we can spin up X opens stack VM to host our containers under script control.   
- C)  Spinning up the containers.   Using the Openstack Nova API.
- D) Managing the life cycle of the containers, 
-  E) Supporting rolling deploys,  
- F) Supporting service upgrades by rebooting containers with new images.

## Extended Goals

* Demonstrate booting a Weblogic 12C container in a LXD container. 
* 



# Architectural Assertions:

* Will use canonical names where possible.
* Our system will start with a list of servers by Name & IP.  It will build images,  distribute the images and start the images across the set of servers.
  * The servers will all be layered so some may be classed as front end while others are middle tier when deploying an app we will spread them around. 
* Will build images in layers where one script builds out and saves an image such as hardened Ubuntu, Then one which saves one with Basic tomcat starting from hardened Ubuntu,  Then one that saves hardened tomcat from basic tomcat.  Each script will start with a previous image and only add it's specialty.   Scripts will be chained to allow entire sequence to be automated.
* I will use the LXD bridged network configuration where possible.    This is in lieu of using network configurations where the container takes over the interface.
  * Only ports forwarded from host will be accessible to outside.
  * Will use Layer 7 routing to containers to route http requests between nodes by URI configuration.
  * 
* I will use a series of small servers pushing LXD containers to them using the REST API.  This will not use the LXD cluster.   
  * May use lxc, command lines to access remote server.
  * May use REST to talk directly to servers.
  * 
* e utilities built will assume 

# Automated Containers Code

* Port forwarding from host to container

* Build basic container hosting a simple Network listener

* Build a basic container hosting tomcat 

* Deploy pre-built container to remote host

* Remember which hosts we have containers deployed to.

* Create a map of which hosts we target for various containers.

* Creating of LBS to layer 5 routing of service requests to the containers.

## LXD Linkage to Openstack

OpenStack and [Light weight containers](https://linuxcontainers.org/lxd/introduction/) LXD are linked through the [Nova project](https://linuxcontainers.org/lxd/getting-started-openstack/).  A container created with the LXD API can be instantiated as either a full VM or LXD and it will be mostly transparent.    This can allow us to build Devops automation scripts using LXD and still leverage OpenStack but it also allows us to run the workloads on bare metal Ubuntu servers and save money.

# Potential GO Utilities Needed

* read the lxc info for a given container to obtain IP Address and update load balancer config.
* Read lxc info for a given container to obtain IP address and update router tables.
* Take a target number of container images and spawn them and then create the router tables based on their listen ports.  
* Take a number of servers in cluster,  copy images to them,  boot named instances on them, obtain IP address and add to router tables.
* Ability to take a pre-configure vm name, modify it's properties based on consul settings and return 



# Examples

## Make a LXD Container Running Apache Tomcat

A script that will create a container named tomcat that is listening on port 8080.   This assumes that your basic Ubuntu host has already been reasonably hardened. 



###### 	lxd is already working

See executable scripts: [produce_updatedUbuntu.sh](../scripts/produce_container_images/produce_updatedUbuntu),  [produce_basicJDK.sh](../scripts/produce_container_images/produce_basicJDK.sh),  [produce_basicTomcat.sh](../scripts/produce_container_images/produce_basicTomcat.sh)

```sh
# If you have not use lxd init before
# then see the section below on getting lxd working

# Launch a Image with name of tomcat
sudo lxc launch ubuntu:18.04 tomcat

# Run root BASH on tomcat image 
# SKIP # lxc exec tomcat -- bash

# Update existing packages 
lxc exec tomcat -- apt-get update
lxc exec tomcat -- apt-get dist-upgrade -y

#  May need to add hardening steps here 
#  since the boot image may not be good enough
#  A better approach is one script that hardens
#  Then saves it under name so we can boot a 
#  pre-hardened image. 

# Install the deafult JDK
lxc exec tomcat -- apt-get install default-jdk --assume-yes

# Add the group tomcat
lxc exec tomcat -- groupadd tomcat

# Adds a user tomcat of the group tomcat
lxc exec tomcat -- useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

# Install Tomcat8 
lxc exec tomcat -- apt install tomcat8  --assume-yes

# Start the tomcat service
lxc exec tomcat -- service tomcat8 start

# Get the IP Address for the container.
lxc info tomcat
# The actual IP address will be listed as 3rd paramter 
# on first line or one that starts with eth0
# In out instance it was 10.106.228.107

# Test to See if our Server is responding 
curl 10.106.228.107:8080
#  Change the IP address to the one reported
#  by lxc info. Tomcat installs on port 8080 by default
#  If it works then you should be seeing some
#  html text from the server.

# Cleanus up after installs
lxc exec tomcat -- apt-get autoremove --purge -y

# Create a snapshot of tomcat with tag of clean
lxc snapshot tomcat clean
lxc publish tomcat/clean --alias ubuntu-tomcat-basic

# Remove my original working image
lxc stop tomcat
lxc delete tomcat

# Run another Tag from first one.
lxc launch ubuntu-tomcat-basic tomcat1

# New container server may have different IP address
# so need to find it.
lxc info tomcat1

# Test to See if our new Server is responding 
# Replace IP address swith the one reported from
# lxc info tomcat1
curl 10.106.228.245:8080
# Should see some html markup.


```

* See Also [Start and stop tomcat from command line](https://www.webucator.com/how-to/how-start-stop-apache-tomcat-from-the-command-line-linux.cfm)
* [Instructions for Apache, neonion, java 8, elastic search, routing with port forwarding](https://github.com/FUB-HCC/neonion/wiki/Hosting-with-LXD)
* 

### 	Adding War to Tomcat running in the Container

You can add as many WAR files as you want as long as they have a unique name.  

```sh
# Assuming you finished the lines above
# Start a new container called tomcat-war
lxc launch ubuntu-tomcat-basic tomcat-war

# Get IP Address of new instance of tomcat-war
lxc info tomcat-war

# Test to be sure the WAR contents are not yet present.
curl 10.106.228.187:8080/sample/index.html
# Assuming you used the IP address from lxc info
# then you should get a stats 404 error message


# Download a WAR file and place it in the correct location on the tomcat server.   
lxc exec tomcat-war --  wget -O /var/lib/tomcat8/webapps/sample.war  https://tomcat.apache.org/tomcat-8.5-doc/appdev/sample/sample.war
# NOTE the WAR file used here was one I felt was safe 
# to directly copy but please use your own verification 
# that you are copying from a trusted source. 

# Re-test to see if the WAR contents have shown up.
curl 10.106.228.187:8080/sample/index.html
# TEst to see if the dynamic JSP also worked
curl 10.106.228.187:8080/sample/hello.jsp

# Save the image so it can be rebooted with the 
# WAR File already added Saved with alias ubuntu-tomcat-war
lxc snapshot tomcat-war clean
lxc publish tomcat-war/clean --alias ubuntu-tomcat-war

# List the images created on the local host. The image 
# published in prior step should show up in this list.
lxc image list


# You can now run more copies of Tomcat with the war
# pre-installed using the new saved name.
lxc launch ubuntu-tomcat-war tomcat-war
```

* [Apache tomcat page ubuntu](https://help.ubuntu.com/lts/serverguide/tomcat.html.en)
* 

### 	Running multiple copies of a server on the same host. 

This could also use the lxd command to run these images on remote servers.   

```sh
lxc launch ubuntu-tomcat-war tomcat-war-1

lxc launch ubuntu-tomcat-war tomcat-war-2

lxc launch ubuntu-tomcat-war tomcat-war-3

lxc launch ubuntu-tomcat-war tomcat-war-4

lxc launch ubuntu-tomcat-war tomcat-war-5

lxc launch ubuntu-tomcat-war tomcat-war-6

See IP for each server
lxc info tomcat-war-1
lxc info tomcat-war-2
lxc info tomcat-war-3
lxc info tomcat-war-4
lxc info tomcat-war-5
lxc info tomcat-war-6

```



### 	Adding a SSL Cert to Tomcat

TODO: 

### 	Changing the Listen Port to Tomcat



```sh
# Lets start with a fresh copy and verify
# it can respond without side effects from last time.
lxc stop tomcat-war
lxc delete tomcat-war
lxc launch ubuntu-tomcat-war tomcat-war
lxc info tomcat-war

# Get the IP Addres from the info and use in curl
# to ensure it works.
curl 10.4.29.246:8080 
# Replace IP for curl with IP from lxc info. 
# Should return some markup.

# Install python interpreter so it is available
# to do local customization inside of the container
lxc exec tomcat-war -- apt install python --assume-yes

# Copy Python script to update server.xml with the new
# listen port.   Note: I copied the file from repository
# into virtual box by putting vim in insert mode and 
# pasting only as a short cut until could save file in 
# predictable location to pull with wget. 
# I could have copied with scp but probably better to just
# make it available via HTTP. 
# Soure file is ../test/update-server-xml.py in this 
# repository. 
lxc file push update-server-xml.py tomcat-war/update-server-xml.py

# Run the python code locally on the container
# because trying to pull it down, modify and push
# back was not working well. 
lxc exec tomcat-war -- python /update-server-xml.py

# Check to see if the timestamp has changed.
lxc exec tomcat-war -- ls -l /etc/tomcat8/server.xml 

# Install golang compiler not really sure if we will
# need it just placed here in case.
#lxc exec tomcat-war -- apt install golang --assume-yes

# Stop the service 
lxc exec tomcat-war -- service tomcat8 stop

# Restart the service should now be listenting on new port
lxc exec tomcat-war -- service tomcat8 start

# Get IP Address from the running version
lxc info tomcat-war

# Test server is responding to the new port
curl 10.4.29.68:8083/sample/hello.jsp

# Reboot a VM to last saved state
#  lxc restart tomcat-war
```

> The original approach of running sed across the lxc exe didn't work because the command line parameters were not all passed correctly.  The approach of pulling the server.xml file over and modifying and pushing it back did not work for unknown reasons.    The approach of saving a python script inside the container and allowing it to modify server.xml worked fine. 
>
> [apache config ubuntu](https://help.ubuntu.com/lts/serverguide/tomcat.html.en)
>
> Default install for web.xml in ubuntu is /etc/tomcat8/web.xml and server.xml, context.xml catalina.properties, logging.properties are in same location.  
>
> [learning linux sed commands to change file in place](https://linuxconfig.org/learning-linux-commands-sed)
>
>

### 	Adding Basic Hardening to the Tomcat Image

TODO:

### Configuring from Host to Container for Tomcat Server

TODO: 

### 	Mapping Multiple copies of same Tomcat image to different host ports:

TODO:  



# Using lxd Proxy to route requests to one of many containers on same host 

When using the default bridged configuration Each LXC container boots onto it's own IP address.   We can discover these IP addresses and then use that knowledge to configure HA Proxy to route the requests received at the proxy level to individual containers.    This is a great security configuration since it is impossible to reach or attack those containers from outside the host and we only expose those ports where we have business need which keeps our attackable surface smaller and limits risk. 

Lxd port forwarding allows an inbound request to a host to be routed to one of the many containers that may be running on the same host.     By forwarding a port to the external interface we can grant access to external callers to interact with the container but only over ports we explicitly allow.   We use the lxd port forwarding but it could also be done using IPTables. 

## lxd port forwarding device 

Carefully triple check the IP addresses and port numbers.  I have tested this both for ssh and http port forwarding and it worked OK.

See:  [lxc port poxy](https://github.com/lxc/lxd/blob/master/doc/containers.md#type-proxy)    

* lxc config device add <container> <device-name> proxy listen=<type>:<addr>:<port>[-<port>][,] connect=<type>:<addr>:<port> bind=<host/container> 

Get your host device IP address

```
ifconfig -a
# Look under the first entry listed for inet address.
```

Add proxy device

```sh
lxc config device add joetest proxy0 proxy listen=tcp:10.0.2.15:8002 connect=tcp:10.41.16.159:22 bind=host

#joetest = name of container
#proxy0 = name of proxy device created
#proxy = type of device created
#192.168.50.196:80  host address listened on
#10.0.10.88:80 target port to receive
#bind=host

```

Remove the Proxy Device

```sh
# Remove device config proxy0 from joetest container
lxc config device remove joetest proxy0
```

Test Access 

```sh
 ssh testuser@127.0.0.1 -p8002
 # Should be able to login and get a ssh prompt if you
 # have configured the ssh access for this container
 # and testuser.  There is a section below that shows
 # how to do that configuration.
```

# Routing inbound requests to Separate containers

## Using HA/Proxy to Route traffic to containers

We have shown elsewhere how to route traffic to from the host network to the container which solves part of the problem.   In our instance we have a number of virtual servers who may have containers running on many hosts.   For uptime and to support rolling deploys we need at least 3 listeners for each service.

As an example our virtual API server may present an interface such as http://person/12828    and http://payments/makePayment    and http://reports/trial-aged?when=today  Since each of these services is fairly complex we want to run each one on a different container.   Depending on the runtime demands for any given service we may or may not not be hosting other containers for other services on the same host.    We do need the flexibility to move high impact / high demand service into containers on different hosts without significant ripples.    When we apply our rules for a minimum of tripple listeners we will have at least 9 containers. 

HAProxy provides load balancing and layer 7 routing services.   We will actually run 2 HA Proxy servers  in containers and use the linux heart-beat functionality to allow one to take over for the other when it is taken down.  This gives us a configuration such as:

Multiple Nodes  computers:

```
  node_1 = 10.0.15.1
  node_2 = 10.0.15.2
  node_3 = 10.0.15.3
  node_4 = 10.0.15.4
  # IP Addresses are only intended as representative
  # you will have to use your own.
```

* This could be from 1 to N computers but most clusters would be from 1 to a few hundred. 
* We will need to dedicate some nodes to specific kinds or groups of tasks to be sure we are not unacceptably mixing concerns.  EG:  We may have 3 nodes dedicated to frontend and we are not allowed to place any containers on those nodes that should not be exposed to mid tier traffic. traffic on those nodes that does not belong.  A good example of this is that we may have separate haproxy on front end routing traffic to those servers but the kvconfig service may only exist behind a separate ha proxy and may only exist on back end higher security zone servers. 
* 

#### Virtual Port / Machine / service routing

| node# | CONTAINER   | LISTEN PORT | EXTERNAL PORT | URIPREFIX      |               |
| ----- | ----------- | ----------- | ------------- | -------------- | ------------- |
| 1     | Person      | 443         | 8020          | /person        |               |
| 1     | MakePayment | 443         | 8021          | /payments/make |               |
| 1     | trialAged   | 443         | 8022          | /reports       |               |
| 1     | haproxy     | 443         | 443           | *              |               |
| 2     | Person      | 443         | 8020          | /person        |               |
| 2     | MakePayment | 443         | 8021          | /payments/make |               |
| 2     | trailAged   | 443         | 8022          | /reports       |               |
| 2     | haproxy     | 443         | 443           | *              |               |
| 3     | person      | 443         | 8020          | /person        |               |
| 3     | makepayment | 443         | 8021          | /payments/make |               |
| 3     | trialaged   | 443         | 8022          | /reports       |               |
| 3     | kvconfig    | 443         | 8023          | /kv/config     | internal only |
| 1     | kvconfig    | 443         | 8023          | /kv/config     | internal only |
| 4     | kvconfig    | 443         | 8023          | /kv/config     | internal only |
|       |             |             |               |                |               |

* Routing to the internal service port to external port is done using lxc proxy device.   The HAProxy only needs to receive it's traffic and route it to one of those listeners on the appropriate exterior port. 

### Building a Container with HAPRoxy

Building the Basic Container

​	See executable script [produce_basicHaproxy.sh](../scripts/produce_container_images/produce_basicHaproxy.sh)

```sh
# Launch a Image with name of haproxy
sudo lxc launch ubuntu:18.04 haproxy

# Run root BASH on haproxy image 
# SKIP # lxc exec haproxy -- bash

# Update existing packages 
lxc exec haproxy -- apt-get update
lxc exec haproxy -- apt-get dist-upgrade -y

# Install the haproxy package
lxc exec haproxy -- apt install -y haproxy

# Show the version of haproxy installed
lxc exec haproxy -- haproxy -v

# socat is needed to support the haproxy
# dynamic configuration API
lxc exec haproxy --  apt install socat

# display contents of the current haproxy.cfg file
lxc exec haproxy -- cat /etc/haproxy/haproxy.cfg

# Now set our default ENV variable so we know this
# is a specific image.  To do this we use some helper 
# scripts we will donwload from this repository.
# Download script
lxc exec haproxy -- curl https://bitbucket.org/joexdobs/devops_lxd_containers/raw/master/scripts/add_if_missing.sh -o "add_if_missing.sh"
# Download script
lxc exec haproxy -- curl https://bitbucket.org/joexdobs/devops_lxd_containers/raw/master/scripts/add_default_env.sh -o "add_default_env.sh"

#  run the script to set the default ENV variable
# for root
lxc exec haproxy -- bash add_default_env.sh 

# Run script to add ENV variable to all enviornments
# when they start
lxc exec haproxy -- bash add_if_missing.sh ENV ENV=
"BUILDIMG\" /etc/environment

# Add the URI where we can expect to find our config store.
# So all shells should inherit it.  Notice the {ENV} which
# intended to be expanded at runtime with the value in the
# ENV enviornment variable.
lxc exec haproxy -- bash add_if_missing.sh KVCONFIG KVCONFIG=\"http://kvconfigstore.\{ENV\}.abc.com\" /etc/environment


#TODO: NEED TO SEE IF THE .bash value saved in 
#  the enviornment variable worked and is set for the 
#  enviornment the service is reading. 

#TODO: Copy the enviornment variable setting to the
#  Tomcat script.

# Start haproxy with the default configuration file.
lxc exec haproxy --  haproxy -f /etc/haproxy/haproxy.cfg       -D -p /var/run/haproxy.pid -sf $(cat /var/run/haproxy.pid)

# Cleanup the image delete old packages
lxc snapshot haproxy clean

# Publish the image as ubuntu-haproxy-basic
# This allows us to use it in the future.
lxc publish haproxy/clean --alias ubuntu-haproxy-basic

# List the images created on the local host. The image 
# published in prior step should show up in this list.
lxc image list


```

Adding SSH To the container

TODO:

Adding  Proxy configuration to HAProxy

* haproxy ip = 10.41.16.50
* TODO: Utility to take a list of machines and lookup their addresses. We will need this to generate the load balancer specification file. 

```
lxc exec haproxy -- ps -ef | grep haproxy
```

```
_single$ lxc exec haproxy -- kill 1178
```

#### Layer 4 - Simple proxy load balancing haproxy

```sh
# Edit the simpleproxy.cfg file and add the next text 
# blurb in next code block to it. 
lxc exec haproxy -- vi /etc/haproxy/haproxy.cfg


# Place the proxy  config in a  separate file so we do
# not have to modify the default config.  We can use 
# two -f options to load both files. 
```

```sh
# Add the following blurb ot the /etc/haproxy/haproxy.cfg
# it should accept a request on our port 8083 and forward
# it to port 8 on the joetest container.   

frontend http_front
  bind *:8083
  stats uri /haproxy?stats
  default_backend http_back

backend http_back
  balance roundrobin
  server joetest 10.41.16.159:8080 check
  # Add more servers here for the default back end.
   
```

```sh
# Check the add on config file for simple load balancing proxy
lxc exec haproxy --  haproxy -c -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/simpleproxy.cfg
```

```sh
#Start a new instance of haproxy
lxc exec haproxy --  haproxy -D -V -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/simpleproxy.cfg
```

```sh
# Restart the HA Proxy Service
lxc exec haproxy -- sudo systemctl restart haproxy
```

```
# Test to see if the config worked
curl 10.41.16.50:8083
```

#### Layer 7 proxy haproxy with prefix matching to choose the back end servers based on URI content.

```sh
# Edit the l7proxy.cfg file and add the next text 
# blurb in next code block to it. 
lxc exec haproxy -- vi /etc/haproxy/l7proxy.cfg
```

```sh
# Add the following blurb ot the /etc/haproxy/haproxy.cfg
# it should accept a request on our port 8083 and forward
# it to port 8 on the joetest container.   

frontend http_front
  bind *:8083
  stats uri /haproxy?stats
  # ACL with prefix of /blog maps to url_blog
  acl url_blog path_beg /blog
  # if request matches url_blog then map to different
  # backend url_blog
  use_backend blog_back if url_blog
  default_backend http_back

backend http_back
  balance roundrobin
  server joetest 10.41.16.159:8080 check
  # Add more servers here for the default back end.

backend blog_back
   server tomcat  10.41.16.65:8080 check
   
```

```sh
#Check config changes to be sure they are valid
lxc exec haproxy --  haproxy -c -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/l7proxy.cfg
```

```sh
#Layer 7 proxy restart for load balancing proxy with prefix matching
lxc exec haproxy --  haproxy -D -V  -f /etc/haproxy/haproxy.cfg -f /etc/haproxy/l7proxy.cfg
```

```sh
# Restart the HA Proxy Service
lxc exec haproxy -- sudo systemctl restart haproxy
```

```sh
# Test to see if the config worked
curl 10.41.16.50:8083
```

#### No Layer 7 Proxy routing without SSL Termination

Haproxy  can load balance at layer 4 because it can make routing decisions based on port #.  This allows it to  just tunnel the SSL through to the end server without inspecting the URI.   Haproxy can not tunnel Layer 7 without terminating the SSL end point and re-encrypting for the next layer of connections you must decrypt traffic inside the TLS session to see the URI.   We are showing the basic configuration above without ssl. 

###### Related Links for HAProxy:

- 

See the [ha proxy links in lxd-reference](lxd-reference.md#HAProxy Reference links) 



* TODO:  
  * Change content available on tomcat server to better test the proxy by prefix.  Add a /blog/index.html file and then we should  see that content when using the blog prefix.
  * Demonstrate prefix matching to route to different server
  * Add script that copies the defined file for the simpleproxy.cfg to it and restarts the server. 
  * Add the front end / back end components based on defined services.
  * Demonstrate haproxy supporting rolling deploy.  haproxy must be restarted to read new config files so it is desirable to avoid config file changes during normal operating conditions including during a rolling deploy.   We will  configure an extra server labeled spare during the original haproxy config and then use runtime api to disable the spare.  During rolling deploy we spin up a new server,  use the runtime api to enable the spare, disable the extra and then do the work to maintain one node.  Re-enable the maintained node,  Repeat the process until done.  Will need to monitor old server until all connections are closed then can actually take it offline.
  * Demonstrate automatic under maintenance page when all members are off line.
  * Demonstrate using a floating IP to provide HA at the load balancer level.
  * Keep this: 

### Configuring our application map

TODO:



### Basic Script to Add Port Forwarding for a new container

TODO:

* Check to see if containerNameProxyPort# exists if so delete it.
* Create a new proxy mapping the desired internal port to the target external port. 
* Quest:  Should define a new unused port in series range such as above 8000 when needed. 
* TODO:  Look at lxc groups to manage communication between groups.

### Script Adding a Basic Proxy Config for HA Proxy

TODO:

### Defining YAML to setup HA Proxy for our Routing

TODO:

### Scripts to create the nodes for each service

TODO: 





## DNS  Configuration for LXD

It can be helpful to take the IP address assigned when we launch a container and modify our host DNS lookup to resolve it by name.   So if we launch container named "joetest" then it would be nice to be able to access it with normal Linux commands such as **ssh joetest** or **ping joetest**.

> Our go utility  dns_register_container -cname=joetest -type=localhost will do the same thing as the manual steps below.

Assuming you have a container named joetest running.  Obtain the IP address from **lxd info joetest** and then modify the /etc/hosts file.  We can also add it for resolution using Dnsmasq but that is overkill for this purpose.  /etc/hosts is  write protected so we must temporarily elevate our permissions.

Add the following line to /etc/hosts

```sh
10.41.16.159 joetest
# LEAVE ALL OTHER LINES UNTOUCHED.
# Assuming that 10.41.16.159 is replaced with container IP
# Assuming that joetest is container name.
```

Starting the Editor

```
sudo vi /etc/hosts
```

Starting File

```SH
127.0.0.1       localhost.localdomain   localhost
::1             localhost6.localdomain6 localhost6
#The following lines are desirable for IPv6 capable hosts

::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
```

Ending File

```sh
127.0.0.1       localhost.localdomain   localhost
::1             localhost6.localdomain6 localhost6
#The following lines are desirable for IPv6 capable hosts

::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts
10.41.16.159 joetest
```

Try it out with:

```
ping joetest
# You should receive a ping result from your container.
```

# Modify lxd container to accept SSH Connections

It may be helpful to allow login via ssh to a container. I find this useful when experimenting with commands before encoding them into scripts.  It can also be helpful to be able to use scp to copy in commands.    This section helps you modify a container to allow password authenticated access to a container.  If you want to access it from outside the host you will also need to forward port 22 to another port on the host.

```sh
lxc exec joetest bash
# Will access a  root Bash console for container
# joetest.

adduser testuser
# This will create the user testuser in the container.
# set the password and fill in other information.

usermod -aG sudo testuser
# This will add testuser to the sudo user group

vi /etc/ssh/sshd_config
# Modify the line from PasswordAuthentication no
# to PasswordAuthentication yes
# Assuming you want to allow test user to login with 
# userid and password rather than security token.

service sshd restart
# Force sshd service to load changes you made in the sshd_config file.
 
exit
# This should return you ot the shell on your host computer

```

Try to access the container again:

```sh
 ssh testuser@joetest
 # change testuser to name of user you want to use when ssh
 # Change joeetst to your container name.
 # Should should see a new prompt something like
 # $testuser@joetest:~$
 # You can also now scp to the container. 
```

TODO: 

* GO Utility to change the password setting.
* Shell script to do the steps above using the lxc exec commands.
* Add instructions here to generate the SSH keys to allow access without userid and password.

* See Also [Steps to create a new Sudo user](https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart)

* 


# Important Leanings

* Some [LXD documentation even that from ubuntu](https://github.com/lxc/lxd/blob/master/doc/rest-api.md#10containersnameexec)  Include reference to commands of style lxc-info rather than the lxd format lxc info When configured as I have LXD on my Ubuntu 18.10 server the older format commands try to run but fail to find the images.  As such I will use the newer command format. 

# Future Research & TODO

* Demonstrate retaining data between lxc deployments.  When we destroy an old container to re-deploy a new container we normally do not care about local data but in the instance such as Elk we would need to retain the existing indexes and we do not want to do this with elevated privileges.
* Find a WebLogic 12 Container or build one that is basically serving a sample WAR file.  (**High Priority**) [installing weblogic 12 on ubuntu server](https://docs.oracle.com/cd/E24329_01/doc.1211/e24492/console.htm#WLSIG205)  [oracle 12 download page](https://www.oracle.com/technetwork/middleware/weblogic/downloads/wls-main-097127.html)  [understanding lxc and docker containers on oracle linux](https://community.oracle.com/docs/DOC-1002057)
* Try the cpu priority in container settings to test a remnants use policy.
* Research the [LXD MAAS](https://lxd.readthedocs.io/en/latest/containers/) integration to allow MAAS to provision all network hosts and take care of host level patching.
* TODO:  Demonstrate multiple listeners on same IP address to allow failover


#### Socket permission error on lxc start

If you get an error:  Error: Get http://unix.socket/1.0: dial unix /var/snap/lxd/common/lxd/unix.socket: connect: permission denied  when trying to start lxc using lxc list or lxc launch then the following worked for me.

```
sudo usermod -a -G lxd  YOURUSERID

newgrp lxd

id

sudo apt-get udpate

sudo apt-get install lxd 

# when it pops up a choice for version choose lattest

lxc list 
# should run without the error

```

## 



# Reference

*  [lxd reference & related links ](lxd-reference.md)
* [devops guiding principals](guiding-principals.md) Guiding principles recommended for all container based devops projects.
* [lxd commands overview](lxd-commands-overview.md) Notes I made when working through various tutorials. 
* [Setting up Ubuntu to support lxd](lxd-ubuntu-setup.md)  Steps we used from a generic ubuntu 18.10 to get lxd up and running. 

