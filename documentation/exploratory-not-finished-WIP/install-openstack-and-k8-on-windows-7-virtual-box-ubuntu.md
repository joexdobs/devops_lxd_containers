# Running Ubuntu on Windows-10 Home

Documents my adventure of trying to get Openstack running Kubernetes running on my Windows laptop using Oracle Virtual Box booted into 64 bit Ubuntu.

I choose this configuration because I wanted to stay as true to the Canonical version of open stack as possible since that is the version of open stack I plan to use these scripts on.   I wanted it on my laptop so I could take it with me and work in my spare time.   On second thought I probably would have been better served using a spare server and starting with native Ubuntu but that would have less convenient.

Note:  While I have a strong preference to install all components purely from script My main intent is to get to where the devops scripts can  run as infrastructure as code.  As such I accepted installation using GUI for lower level components such as Virtual Box and the Open stack Core.  IN most cases DevOps we would be starting with a working Openstack appliance anyway.

NOTE:  Kubernetes is also available as a charm directly on the Ubuntu server.  It may be reasonable to simply run the K8 commands when starting out then come back and add the scripts to create the OpenStack VM's.    Due to limited hardware resources I may fallback to this approach.

At a high level this process involves:

1. [Install the Virtualbox VM on Windows](Installing Virtual Box) - This is needed because Windows home does not include a virtual machine manager. 
2. [Start the Openstack virtual machine running Ubuntu](Booting Ubuntu on Virtual Box)
3. [Install Openstack on the Ubuntu server using Conjur Up](Installing Openstack on the Ubuntu box)
4. Install K8 on the Openstack



## Installing K8 on Ubuntu 

* My juju for ubuntu failed so falling back to using sudo aptget.

* ```
  sudo apt-get update && apt-get install -y apt-transport-https
  ```

* https://docs.docker.com/config/daemon/#start-the-daemon-using-operating-system-utilities

* See: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1    

* See: https://www.techrepublic.com/article/how-to-quickly-install-kubernetes-on-ubuntu/  Skip the docker config in favour instructions from the docker site in prior step but the kubernetes steps seem to work OK. 



* ```
  sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add 
  Next add a repository by creating the file /etc/apt/sources.list.d/kubernetes.list and enter the following content:
  
  deb http://apt.kubernetes.io/ kubernetes-xenial main 
  Save and close that file. Install Kubernetes with the following commands:
  
  apt-get update
  apt-get install -y kubelet kubeadm kubectl kubernetes-cni
  
  sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
  
  sudo kubeadm init
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
  sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
  sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml
  sudo kubeadm join --token TOKEN MASTER_IP:6443
  sudo kubectl get nodes
  sudo kubectl run --image=nginx nginx-app --port=80 --env="DOMAIN=cluster"
  sudo kubectl expose deployment nginx-app --port=80 --name=nginx-http
  
  
  ```

* ```
  deb http://apt.kubernetes.io/ kubernetes-xenial main 
  ```

## Installing K8 on Openstack

* 

## Installing Openstack on the Ubuntu box

See Also: [Openstack workstation on Ubuntu site](https://www.ubuntu.com/openstack/install#workstation-deployment).

Install LXD driver because we can only use the lxd containers in openstack for local workstation after lxd has been configured and a storage pool is available.

```
sudo snap install lxd
/snap/bin/lxd init
```

See Also: https://docs.conjure-up.io/devel/en/user-manual#users-of-lxd  for details on configuring lxd storage pools and networking.

```
jwork@openstackv002:~$ conjure-up
jwork@openstackv002:~$ sudo snap install lxd
snap "lxd" is already installed, see 'snap help refresh'
jwork@openstackv002:~$ /snap/bin/lxd init
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]:
Name of the storage backend to use (btrfs, ceph, dir, lvm, zfs) [default=zfs]:
Create a new ZFS pool? (yes/no) [default=yes]:
Would you like to use an existing block device? (yes/no) [default=no]:
Size in GB of the new loop device (1GB minimum) [default=49GB]:
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]:
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
Would you like LXD to be available over the network? (yes/no) [default=no]: yes
Address to bind LXD to (not including port) [default=all]:
Port to bind LXD to [default=8443]:
Trust password for new clients:
Again:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```

Use conjure-up to install openstack on local host   Runs Conjure-up at command line then follow the prompts.  See Also: [Conjure-up users Manual](https://docs.conjure-up.io/devel/en/user-manual#users-of-lxd)

* ![consure-up openstack](C:\jsoft\devops-k8-openstack\documentation\img\conjure-up-install-openstack-with-novakvm.jpg)
* Waiting for Juju controller to initialize.  I selected all applications to be configured and chose the default network bridge and default storage pool
* Bootstrapping seems to take a while.   It was still working after 5 minutes.    Came back 2 hours latter and it had completed. 
* ![Failed install](C:\jsoft\devops-k8-openstack\documentation\img\openstack-juju-failed.jpg)



# Alternative Using Mirantis Virtual Box

This version can be nice because it pre-installs Openstack with the basic boot image.   It also installs some network port forwarding and other things that are necessary to get Openstack up an running.   

- Download the ISO and scripts
- Unzip the scripts.
- Copy the ISO file into the iso directory the unzip created
- Make sure you have a current version of cygwin installed.   Check to make sure that you have all the required packaged named in the readme file created when you unziped.      The basic list are:  procps, expect, openssh-client, xxd, etc.
- install [virtual box extension pack](https://www.virtualbox.org/wiki/Downloads) from virtualbox website.



## Booting Ubuntu on Virtual Box

* Read the following:  [Port forwarding in virtualbox](https://stackoverflow.com/questions/5906441/how-to-ssh-to-a-virtualbox-guest-externally-through-a-host)  You need to configure port forwarding to allow a SSH terminal with full cut and paste capability or you have to interact with the vbox console which is a pain.   At the least you need to forward port 22. 

    * To Open a SSH terminal into the new virtual machine we use port forwarding.  Where we open a specific port on the given machine and it is mapped onto a port in the virtual machine.  In our instance I chose to map port 8001 on the host to port 22 on the virtual machine which allows me SSH acces using a more capable terminal. 

    * ```sh
        #In cygwin sh console 
        ssh jwork@127.0.0.1 -p 8001
        ```

* See Also: [Install Ubuntu server instructions](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-server#0)

* Choose Virtual Box option to create a new Machine and then ![Start Ubuntu Image](C:\jsoft\devops-k8-openstack\documentation\img\create-virtual-machine-virtual-box-ubuntu.jpg)

    With 12000 MB RAM on a 16GB Laptop.  The 4096 version ran too slow to be useful. 

    virtual box VDI Image disk dynamically sized

    Name is Openstack

    Initial hard disk 250GB

    ![](C:\jsoft\devops-k8-openstack\documentation\img\openstack-image-available-to-start.jpg)


1. Download Ubuntu ISO image](https://www.ubuntu.com/download/server)  Save the image in directory where VirtualBox is looking for it.   I chose the 18.04 version.    Images pre-configured for use with virtual box are available at [osboxes.org](https://www.osboxes.org/ubuntu/)   
   See also: [How to install Ubuntu on virtual box](https://www.wikihow.com/Install-Ubuntu-on-VirtualBox)
2. ![Choose the Boot ISO Image for Ubuntu](C:\jsoft\devops-k8-openstack\documentation\img\choose-iso-for-ubuntu-boot-virtual-box.jpg) 
3. Choose the option to Start
4. Server will go through the boot process and ask standard install questions.

   1. Choose English
   2. When it presents install options choose install Ubuntu
   3. ![Choose the Default Ethernet adapter](C:\jsoft\devops-k8-openstack\documentation\img\choose-default-network-adapter-for-ubuntu.jpg)
   4.  
5. Chose the rest as defaults but I did install docker and conjur-up snaps.
6. After this it took about minutes to finish churning through and present Reboot Now option.
7. 
8. 
9. .
10. .
11. .
12. .
13. .
14. .
15. 



## Installing Virtual Box

- [Download Virtual Box binary](https://www.virtualbox.org/wiki/Downloads)  available for windows, OS X, Linux and Solaris.     Installed with the defaults on Dell XPS13 running Windows 10 Home with 16GB RAM, I7-6560

  The install when easy and I accepted the defaults.   

   After this step see [Installing Ubuntu on Virtual Box](Booting Ubuntu on Virtual Box)


# See Also:

* [Mirantis - installation of Openstack using VirtualBox](https://www.mirantis.com/how-to-install-openstack/) 